# Ratey #

A simple iOS Swift class to manage and display a user rating dialog.

### Features ###

* Easy to use
* Supports iOS 9+ and Swift 4

### How do I get set up? ###

#### Option 1 - Install via Carthage ####

Simply add the following line to your Cartfile:

	git "https://richb8@bitbucket.org/richb8/rbratey.git"

And run:

	carthage update rbratey

#### Option 2 - Clone the repo and copy the source file ####

Simply copy the Ratey.swift file into your application


### Usage ##

* Set the appId property to the ID (e.g. id123456) of your application
* Call the trackUsage() function on the singleton instance
* Be aware, the rate me link will not open if you are debugging using simulator. Please use a physical device.

### Sample Code ###

    Ratey.sharedInstance.appId = "[Your App Id]"
    Ratey.sharedInstance.trackUsage()


### Contact ###

* If you have any questions, feel free to contact us at [hello@rbcoding.co.uk](Link URL) or visit http://rbcoding.co.uk

### License ###


Copyright (c) 2017 RB Coding

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.