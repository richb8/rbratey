//
//  Ratey.swift
//  RBRatey
//
//  Created by Richard Bate on 21/11/2017.
//  Copyright © 2017 Richard Bate. All rights reserved.
//

import Foundation

//
//  Ratey.swift
//  Ratey
//
//  Created by Richard Bate on 04/01/2017.
//
//  Copyright (c) 2017 RB Coding
//
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software.
//
//  Permission is granted to anyone to use this software for any purpose,
//  including commercial applications, and to alter it and redistribute it
//  freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not
//  claim that you wrote the original software. If you use this software
//  in a product, an acknowledgment in the product documentation would be
//  appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be
//  misrepresented as being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//
//

import UIKit


public class Ratey : UIViewController, UIAlertViewDelegate{
    
    // MARK: Consts
    public let kAppVersion = "kAppVersion"
    
    fileprivate let kFirstUseDate = "kFirstUseDate"
    fileprivate let kHasRated = "kHasRated"
    fileprivate let kHasDeclined = "kHasDeclined"
    
    fileprivate let kRemindLater = "kRemindLater"
    fileprivate let kRemindLaterDate = "kRemindLaterDate"
    fileprivate let kAppUseCount = "kAppUseCount"
    
    
    // MARK: Properties
    
    fileprivate var reviewUrl = "itms-apps://itunes.apple.com/app/id"
    
    public static let sharedInstance = Ratey()
    
    public var maxUsesCountBeforePrompt = 10
    public var maxDaysCountBeforePrompt = 30
    public var remindInDaysCount = 1
    public var alertTitle = NSLocalizedString("Rate the app", comment: "Ratey")
    public var alertMessage = "Like this app? Please spend a minute to rate me!"
    public var alertOKTitle = NSLocalizedString("Rate it now", comment: "Ratey")
    public var alertCancelTitle = NSLocalizedString("No thanks", comment: "Ratey")
    public var alertRemindLaterTitle = NSLocalizedString("Remind me later", comment: "Ratey")
    
    public var appId = "";
    
    // MARK: Initialization
    
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    fileprivate override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
    }
    
    
    // MARK: Public funcs
    
    public func trackUsage(){
        
        
        
        if(appId.count == 0){
            fatalError("Please set the appId property to the AppStore ID of your application.")
        }
        
        
        if(isFirstUse()){
            initialiseSettings();
        }
        else{
            incrementAppCount();
        }
        
        
        if(shouldPromptUser()){
            DispatchQueue.main.async(execute: { () -> Void in
                self.showRateyAlert()
            })
        }
    }
    
    
    
    // MARK: Private funcs
    
    fileprivate func initialiseSettings(){
        let settings = UserDefaults.standard
        
        settings.set(getAppVersion(), forKey: kAppVersion)
        settings.set(false, forKey: kHasRated)
        settings.set(false, forKey: kHasDeclined)
        settings.set(false, forKey: kRemindLater)
        settings.set(Date(), forKey: kFirstUseDate)
        settings.set(1, forKey: kAppUseCount)
        
    }
    
    fileprivate func incrementAppCount(){
        let settings = UserDefaults.standard
        
        let currentCount = settings.integer(forKey: kAppUseCount)
        
        settings.set(currentCount + 1, forKey: kAppUseCount)
    }
    
    
    fileprivate func isFirstUse()->Bool{
        
        let settings = UserDefaults.standard
        
        let appVersion = settings.object(forKey: kAppVersion) as? String
        
        if((appVersion == nil) || !(getAppVersion() == appVersion))
        {
            return true
        }
        
        return false
        
    }
    
    
    fileprivate func shouldPromptUser() -> Bool{
        
        let settings = UserDefaults.standard
        
        
        let hasDeclined = settings.bool(forKey: kHasDeclined)
        if(hasDeclined)
        {
            return false
        }
        
        let hasRated = settings.bool(forKey: kHasRated)
        if(hasRated)
        {
            return false
        }
        
        let remindLater = settings.bool(forKey: kRemindLater)
        if(remindLater)
        {
            let remindLaterDate = settings.object(forKey: kRemindLaterDate) as! Date
            
            let remindLaterDaysCount = getDaysBetweenDates(startDate: remindLaterDate, endDate: Date())
            
            return (remindLaterDaysCount >= remindInDaysCount)
        }
        
        let usageCount = settings.integer(forKey: kAppUseCount)
        if(usageCount >= maxUsesCountBeforePrompt)
        {
            return true
        }
        
        let firstUseDate = settings.object(forKey: kFirstUseDate) as! Date
        let daysSinceFirstUse = getDaysBetweenDates(startDate: firstUseDate, endDate: Date())
        
        if(daysSinceFirstUse >= maxDaysCountBeforePrompt)
        {
            return true
        }
        
        
        return false
        
    }
    
    fileprivate func showRateyAlert() {
        
        let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: alertOKTitle, style: UIAlertActionStyle.default, handler: { action in
            
            self.okPressed()
            
            
        }))
        
        alert.addAction(UIAlertAction(title: alertRemindLaterTitle, style: UIAlertActionStyle.default, handler: { action in
            
            self.remindLaterPressed()
            
            
        }))
        
        alert.addAction(UIAlertAction(title: alertCancelTitle, style: UIAlertActionStyle.cancel, handler: { action in
            
            self.declinePressed()
            
            
        }))
        
        
        if let controller = getTopMostViewController(){
            controller.present(alert, animated: true, completion: nil)
        }
        
        
    }
    
    fileprivate func okPressed(){
        
        UserDefaults.standard.set(true, forKey: kHasRated)
        
        guard let appStoreURL = URL(string : reviewUrl + appId) else {
            return
        }
        
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(appStoreURL, options: [:], completionHandler: nil)
            
        } else {
            UIApplication.shared.openURL(appStoreURL)
        }
        
        
    }
    
    fileprivate func remindLaterPressed(){
        let settings = UserDefaults.standard
        settings.set(true, forKey: kRemindLater)
        settings.set(Date(), forKey: kRemindLaterDate)
    }
    
    fileprivate func declinePressed(){
        UserDefaults.standard.set(true, forKey: kHasDeclined)
    }
    
    
    fileprivate func getAppVersion() -> String?{
        
        if let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
            return version
        }
        
        return nil
    }
    
    
    fileprivate func getDaysBetweenDates(startDate: Date, endDate: Date) -> Int
    {
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: startDate, to: endDate)
        return components.day!
    }
    
    
    fileprivate func getTopMostViewController() -> UIViewController? {
        
        var topController: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        
        while (topController.presentedViewController != nil) {
            topController = topController.presentedViewController!
        }
        
        return topController
    }
    
    
}
